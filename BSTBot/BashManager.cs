﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace BSTBot
{
    public static class BashManager
    {
        const string path = "/home/bst/_scripts";
        //const string path = @"C:\Users\Kip\FileZilla downloads";
        public static string ListAll()
        {
            var files = Directory.GetFiles(path, "*.sh");
            string finalMessage = "";
            foreach (var x in files)
            {
                finalMessage += $"{Path.GetFileNameWithoutExtension(x)}\n";
            }

            return finalMessage;
        }

        public static string Run(string bashfile)
        {
            var escapedArgs = bashfile.Replace("\"", "\\\"");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            string result = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            return result;

            //return $"running: \n\n\n{bashfile}";
        }

        public static string Read(string name)
        {
            return File.ReadAllText(path + @"/" + name + ".sh");
        }
    }
}