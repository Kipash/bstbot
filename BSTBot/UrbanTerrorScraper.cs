﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;

public class UrbanTerrorScraper
{
    public static List<Server> Servers = new List<Server>();
    public static List<Player> Players = new List<Player>();

    static List<Server> tempServers = new List<Server>();
    static List<Player> tempPlayers = new List<Player>();

    public static void LoadAllServers()
    {
        using (WebClient client = new WebClient())
        {
            string masterList = client.DownloadString("https://www.urbanterror.info/servers/master/?empty=0");

            Regex ipPattern = new Regex(@"\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}:\d{1,5}");
            var matches = ipPattern.Matches(masterList);

            if(matches.Count == 0)
            {
                Console.WriteLine("ERROR: no data from master list");
            }
            else
            {
                Console.WriteLine($"masterList list: {matches.Count}");
            }

            foreach (Match x in matches)
            {
                tempServers.Add(new Server(x.Value));
            }
        }

        if (Servers.Count == 0)
            Servers = tempServers;
    }

    public static void CreateServerInfo(Server s)
    {
        using (WebClient client = new WebClient()) // WebClient class inherits IDisposable
        {

            var address = s.Address;

            string server = client.DownloadString($"https://www.urbanterror.info/servers/{address}/");
            var doc = new HtmlDocument();
            doc.LoadHtml(server);

            //foreach (var x in doc.ParseErrors)
            //{
            //    Console.WriteLine($"err: {x.SourceText}");
            //}

            var boardPath = @"//center/div/div[@id='board']/div[@id='main']/div[@id='mainBox']";
            var boardNode = doc.DocumentNode.SelectSingleNode(boardPath);
            if (boardNode == null)
            {
                Console.WriteLine("BoardNode was missing on: " + s.Address);
                return;
            }

            var nameNode = boardNode.SelectSingleNode("h2");
            if (nameNode == null)
            {
                Console.WriteLine("NameNode was missing on: " + s.Address);
                return;
            }

            s.Name = nameNode.InnerText;
            //Console.WriteLine($"Server name: {s.Name}");
            //Console.WriteLine($"IP: {address}");

            var playerBoard = boardNode.SelectSingleNode("div[@class='darkerBox']/table[@class='list']");

            if (playerBoard != null)
            {
                //Console.WriteLine($"Players: ");
                var players = playerBoard.Elements("tr");
                for (int i = 1; i < players.Count(); i++)
                {
                    var player = players.ElementAt(i);
                    var plname = player.ChildNodes[3];
                    //Console.WriteLine(">  " + plname.InnerText);
                    tempPlayers.Add(new Player() { Name = plname.InnerText, Server = s });
                }
            }
            else
            {
                Console.WriteLine("Players: Empty - on " + s.Address);
            }
        }
    }

    public static List<Player> FindPlayers(string querry)
    {
        var list = new List<Player>();

        querry = querry.ToLower();
        foreach (var x in Players)
        {
            if (x.Name.ToLower().Contains(querry))
            {
                list.Add(x);
            }
        }

        return list;
    }
    
    public static void ScraperRoundEnd()
    {
        Servers = tempServers;
        Players = tempPlayers;

        tempServers = new List<Server>();
        tempPlayers = new List<Player>();
    }
}

public class Player
{
    public string Name;
    public Server Server;

    public string SanetizedName { get { return SanetizeName(Name); } }

    string SanetizeName(string name)
    {
        if (!string.IsNullOrWhiteSpace(name) && name.Length > 0)
        {
            name = name.Replace("`", @"\`");
            //name = name.Replace("*", @"\*");
            //name = name.Replace("_", @"\_");
            //name = name.Replace("^1", @"");
            //name = name.Replace("^2", @"");
            //name = name.Replace("^3", @"");
            //name = name.Replace("^4", @"");
            //name = name.Replace("^5", @"");
            //name = name.Replace("^6", @"");
            //name = name.Replace("^7", @"");
            //name = name.Replace("^8", @"");
            //name = name.Replace("^9", @"");
        }
        return name.Length > 0 ? name : "Unnamed player";
    }
}
public class Server
    {
        public string Address { get; private set; }
        public string Name;

        public Server(string address)
        {
            Address = address;
        }
    }