﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace BSTBot
{
    class Program
    {
        DiscordSocketClient client;
        CommandService commands;
        IServiceProvider services;

        static void Main(string[] args)
        {
            Console.WriteLine("Starting BST bot");

            var p = new Program();
            p.RunBotAsync()
             .GetAwaiter()
             .GetResult();
        }

        public async Task RunBotAsync()
        {
            client = new DiscordSocketClient();
            commands = new CommandService();

            services = new ServiceCollection()
                .AddSingleton(client)
                .AddSingleton(commands)
                .BuildServiceProvider();

            string token = "NjU1ODA3Mzg5MTEwNTAxMzg4.XgcmJw.ePxLRxSDhzerjvBpmaUZz0Pfnw8";

            client.Log += Client_Log;
            
            await RegisterCommandsAsync();
            await client.LoginAsync(TokenType.Bot, token);
            await client.StartAsync();

            await RunScraperAsync();
            await Task.Delay(-1);
        }

        public async Task RunScraperAsync()
        {
            while (true)
            {
                CW("Loading map list", ConsoleColor.DarkYellow);
                UrbanTerrorScraper.LoadAllServers();
                await Task.Delay(3000);

                CW("Scraping servers...", ConsoleColor.DarkGreen);
                foreach (var s in UrbanTerrorScraper.Servers)
                {
                    UrbanTerrorScraper.CreateServerInfo(s);
                    await Task.Delay(3000);
                }

                CW("Scraping ended, waiting", ConsoleColor.DarkMagenta);
                UrbanTerrorScraper.ScraperRoundEnd();
                await Task.Delay(60000);
            }
            Console.WriteLine("Scraper escaped");
        }

        private Task Client_Log(LogMessage arg)
        {
            Console.WriteLine(arg);
            return Task.CompletedTask;
        }

        public async Task RegisterCommandsAsync()
        {
            client.MessageReceived += HandleCommandAsync;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly(), services);
        }

        public async Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            var context = new SocketCommandContext(client, message);

            if (message.Author.IsBot) return;

            int argPos = 0;
            if (message.HasStringPrefix("$", ref argPos))
            {
                var result = await commands.ExecuteAsync(context, argPos, services);
                if (!result.IsSuccess) Console.WriteLine(result.ErrorReason);
            }
        }

        void CW(string msg, ConsoleColor col)
        {
            var currCol = Console.ForegroundColor;
            Console.ForegroundColor = col;
            Console.WriteLine(msg);
            Console.ForegroundColor = currCol;
        }
    }
}
