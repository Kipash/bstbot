﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace BSTBot.Commands
{
    public class Commands : ModuleBase<SocketCommandContext>
    {
        readonly CommandService service;

        public Commands(CommandService service)
        {
            this.service = service;
        }

        [Command("ping")]
        [Summary("Test command")]
        public async Task Ping()
        {
            await ReplyAsync("Pong");
        }

        [Command("list")]
        [Summary("List all aviable .sh files")]
        public async Task List()
        {
            await ReplyAsync(BashManager.ListAll());
        }

        [Command("run")]
        [Summary("Run a specific .sh file")]
        [RequireUserPermission(GuildPermission.ManageMessages)]
        public async Task List(string name)
        {
            string bash = BashManager.Read(name);
            string response = $"```{BashManager.Run(bash)}```";

            await ReplyAsync(string.IsNullOrWhiteSpace(response) ? "The command didn't return any result!" : response);
        }

        [Command("find")]
        [Summary("Find online players")]
        public async Task Find(string qurry)
        {
            var list = UrbanTerrorScraper.FindPlayers(qurry);
            string msg = "**Players: **\n";
            if (list.Count > 0 && list.Count < 5)
            {
                foreach(var x in list)
                {
                    msg += $"Found `{x.SanetizedName,-20}` on `{x.Server.Name,-30}` (urt:{"\\"}{x.Server.Address})\n";
                }
            }
            else if (list.Count >= 5)
            {
                msg += "**Too many results!!**";
            }
            else
            {
                msg += "**No players found**";
            }

            var lines = msg.Split('\n');
            foreach (var l in lines)
            {
                await ReplyAsync(l);
            }
        }

        [Command("scraper")]
        [Summary("List scraper info")]
        public async Task ScraperInfo()
        {
            string msg = $"Players: {UrbanTerrorScraper.Players.Count}\nServers: {UrbanTerrorScraper.Servers.Count}";
            await ReplyAsync(msg);
        }


        [Command("help")]
        [Summary("Get all available commands")]
        public async Task HelpAsync()
        {
            string prefix = "$";
            var builder = new EmbedBuilder()
            {
                Color = new Color(206, 206, 30),
                Description = "**Available commands:**"
            };

            foreach (var module in service.Modules)
            {
                string description = null;
                foreach (var cmd in module.Commands)
                {
                    var result = await cmd.CheckPreconditionsAsync(Context);
                    if (result.IsSuccess)
                        description += $"{prefix}{cmd.Aliases.First()}\n";
                }

                if (!string.IsNullOrWhiteSpace(description))
                {
                    builder.AddField(x =>
                    {
                        x.Name = module.Name;
                        x.Value = description;
                        x.IsInline = false;
                    });
                }
            }

            await ReplyAsync("", false, builder.Build());
        }
        /*
        [Command("help")]
        [Summary("Get Help for specific command")]
        public async Task HelpAsync(string command)
        {
            var result = service.Search(Context, command);

            if (!result.IsSuccess)
            {
                await ReplyAsync($"Sorry, I couldn't find a command like **{command}**.");
                return;
            }

            string prefix = "!";
            var builder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Description = $"Here are some commands like **{command}**"
            };

            foreach (var match in result.Commands)
            {
                var cmd = match.Command;

                builder.AddField(x =>
                {
                    x.Name = string.Join(", ", cmd.Aliases);
                    x.Value = $"Parameters: {string.Join(", ", cmd.Parameters.Select(p => p.Name))}\n" +
                              $"Summary: {cmd.Summary}";
                    x.IsInline = false;
                });
            }

            await ReplyAsync("", false, builder.Build());
        }
        */
    }
}
